# purpose: this program finds the maximum number of a set of data items.

# variables: the registers have the following uses:
# %edi - holds the index of the data item being examined
# %ebx - largest data item found
# %eax - current data item

# data_items - contains the item data. A 0 is used to terminate the data

.section .data
data_items:
.long 3, 64, 34, 37, 0

.section .text
.globl _start
_start:
movl $0, %edi
movl data_items(,%edi,4), %eax
movl %eax, %ebx

start_loop:
cmpl $0, %eax
je loop_exit
incl %edi
movl data_items(,%edi, 4), %eax
cmpl %ebx, %eax
jle start_loop
movl %eax, %ebx
jmp start_loop

loop_exit:
movl $1, %eax
int $0x80
