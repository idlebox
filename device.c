#include <string.h>
#include <sys/socket.h>
#include <linux/netlink.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include "device.h"
#include "log.h"

int device_init(void)
{
	int fd;

	fd = open_uevent_socket();
	if(fd < 0)
		return -1;

	fcntl(fd, F_SETFD, FD_CLOEXEC);
	fcntl(fd, F_SETFL, O_NONBLOCK);

	coldboot(fd, "/sys/class");
	coldboot(fd, "/sys/block");
	coldboot(fd, "/sys/devices");

	return fd;
}   

int open_uevent_socket(void)
{
	struct sockaddr_nl addr;
	int sz = 64*1024;
	int s;

	memset(&addr, 0, sizeof(addr));
	addr.nl_family = AF_NETLINK;
	addr.nl_pid = getpid();
	addr.nl_groups = 0xffffffff;

	s = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_KOBJECT_UEVENT);
	if(s < 0)
		return -1;

	setsockopt(s, SOL_SOCKET, SO_RCVBUFFORCE, &sz, sizeof(sz));

	if(bind(s, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
		close(s);
		return -1;
	}

	return s;
}

void coldboot(int event_fd, const char *path)
{
	DIR *d = opendir(path);
	if(d) {
		do_coldboot(event_fd, d);
		closedir(d);
	}
}

void do_coldboot(int event_fd, DIR *d)
{
	struct dirent *de;
	int dfd, fd;

	dfd = dirfd(d);

	fd = openat(dfd, "uevent", O_WRONLY);
	if(fd >= 0) {
		write(fd, "add\n", 4);
		close(fd);
		handle_device_fd(event_fd);
	}

	while((de = readdir(d))) {
		DIR *d2;

		if(de->d_type != DT_DIR || de->d_name[0] == '.')
			continue;

		fd = openat(dfd, de->d_name, O_RDONLY | O_DIRECTORY);
		if(fd < 0)
			continue;

		d2 = fdopendir(fd);
		if(d2 == 0)
			close(fd);
		else {
			do_coldboot(event_fd, d2);
			closedir(d2);
		}
	}
}

#define UEVENT_MSG_LEN  1024
void handle_device_fd(int fd)
{
	char msg[UEVENT_MSG_LEN+2];
	int n;

	while((n = recv(fd, msg, UEVENT_MSG_LEN, 0)) > 0) {
		struct uevent uevent;

		if(n == UEVENT_MSG_LEN)   /* overflow -- discard */
			continue;

		msg[n] = '\0';
		msg[n+1] = '\0';

		parse_event(msg, &uevent);

		handle_device_event(&uevent);
		//handle_firmware_event(&uevent);
	}
}

void parse_event(const char *msg, struct uevent *uevent)
{
	uevent->action = "";
	uevent->path = "";
	uevent->subsystem = "";
	uevent->firmware = "";
	uevent->major = -1;
	uevent->minor = -1;

	/* currently ignoring SEQNUM */
	while(*msg) {
		if(!strncmp(msg, "ACTION=", 7)) {
			msg += 7;
			uevent->action = msg;
		} else if(!strncmp(msg, "DEVPATH=", 8)) {
			msg += 8;
			uevent->path = msg;
		} else if(!strncmp(msg, "SUBSYSTEM=", 10)) {
			msg += 10;
			uevent->subsystem = msg;
		} else if(!strncmp(msg, "FIRMWARE=", 9)) {
			msg += 9;
			uevent->firmware = msg;
		} else if(!strncmp(msg, "MAJOR=", 6)) {
			msg += 6;
			uevent->major = atoi(msg);
		} else if(!strncmp(msg, "MINOR=", 6)) {
			msg += 6;
			uevent->minor = atoi(msg);
		}

		/* advance to after the next \0 */
		while(*msg++)
			;
	}

	/*INFO("event { '%s', '%s', '%s', '%s', %d, %d }\n",
			uevent->action, uevent->path, uevent->subsystem,
			uevent->firmware, uevent->major, uevent->minor);*/
}

void handle_device_event(struct uevent *uevent)
{
	char devpath[96];
	char *base, *name;
	int block;

	/* if it's not a /dev device, nothing to do */
	if((uevent->major < 0) || (uevent->minor < 0))
		return;

	/* do we have a name? */
	name = strrchr(uevent->path, '/');
	if(!name)
		return;
	name++;

	/* too-long names would overrun our buffer */
	if(strlen(name) > 64)
		return;

	/* are we block or char? where should we live? */
	if(!strncmp(uevent->subsystem, "block", 5)) {
		block = 1;
		base = "/dev/block/";
		mkdir(base, 0755);
	} else {
		block = 0;
		/* this should probably be configurable somehow */
		if(!strncmp(uevent->subsystem, "graphics", 8)) {
			base = "/dev/graphics/";
			mkdir(base, 0755);
		} else if (!strncmp(uevent->subsystem, "oncrpc", 6)) {
			base = "/dev/oncrpc/";
			mkdir(base, 0755);
		} else if (!strncmp(uevent->subsystem, "adsp", 4)) {
			base = "/dev/adsp/";
			mkdir(base, 0755);
		} else if (!strncmp(uevent->subsystem, "msm_camera", 10)) {
			base = "/dev/msm_camera/";
			mkdir(base, 0755);
		} else if(!strncmp(uevent->subsystem, "input", 5)) {
			base = "/dev/input/";
			mkdir(base, 0755);
		} else if(!strncmp(uevent->subsystem, "mtd", 3)) {
			base = "/dev/mtd/";
			mkdir(base, 0755);
		} else if(!strncmp(uevent->subsystem, "sound", 5)) {
			base = "/dev/snd/";
			mkdir(base, 0755);
		} else if(!strncmp(uevent->subsystem, "misc", 4) &&
				!strncmp(name, "log_", 4)) {
			base = "/dev/log/";
			mkdir(base, 0755);
			name += 4;
		} else
			base = "/dev/";
	}

	snprintf(devpath, sizeof(devpath), "%s%s", base, name);

	if(!strcmp(uevent->action, "add")) {
		make_device(devpath, block, uevent->major, uevent->minor);
		return;
	}

	if(!strcmp(uevent->action, "remove")) {
		unlink(devpath);
		return;
	}
}

void make_device(const char *path, int block, int major, int minor)
{
	unsigned uid;
	unsigned gid;
	mode_t mode;
	dev_t dev;

	if(major > 255 || minor > 255)
		return;

	mode = get_device_perm(path, &uid, &gid) | (block ? S_IFBLK : S_IFCHR);
	dev = (major << 8) | minor;
	mknod(path, mode, dev);
	chown(path, uid, gid);
}

mode_t get_device_perm(const char *path, unsigned *uid, unsigned *gid)
{
	return 0777;
}
