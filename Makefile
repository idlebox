uname_S = $(shell sh -c 'uname -s 2>/dev/null || echo not')
uname_O = $(shell sh -c 'uname -o 2>/dev/null || echo not')

# compiler enviroment
MKDIR = mkdir
CC = gcc
CPPFLAGS = -D_GNU_SOURCE
CFLAGS = -g -static -Wall
LDFLAGS = -g -static -Wall
ifeq ($(uname_O), Cygwin)
	CFLAGS = -g -Wall
	LDFLAGS = -g -Wall
endif
LDLIBS = 

# project setting
NODEPS = clean
OUTDIR = out# only support current directory now :(
SOURCES = $(wildcard *.c)
TARGET = $(OUTDIR)/init
PACKAGE = $(OUTDIR)/rootfs.cpio.gz
OBJECTS = $(patsubst %.c, $(OUTDIR)/%.o, ${SOURCES})
DEPFILES = $(patsubst %.c, $(OUTDIR)/%.d, ${SOURCES})

# link target
$(TARGET):$(OBJECTS)
	@$(MKDIR) -p $(OUTDIR)
	$(CC) $(LDFLAGS) $^ -o $@

$(PACKAGE) : $(TARGET)
	@rm -rf out/package; \
	mkdir -p out/package; \
	cd out/package; \
	cp ../init .; \
	find . | cpio -o -H newc | gzip > ../rootfs.cpio.gz

# dependency
#ifneq ($(MAKECMDGOALS), clean)
-include $(DEPFILES)
#endif

#$(OUTDIR)/%.d:%.c
#	@$(MKDIR) -p $(OUTDIR)
#	set -e; rm -f $@; \
#	$(CC) -MM $(CPPFLAGS) $< > $@.$$$$; \
#	sed 's,\($*\)\.o[ :]*,$(OUTDIR)/\1.o $@ : ,g' < $@.$$$$ > $@; \
#	sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
#	    -e '/^$$/ d' -e 's/$$/ :/' < $@.$$$$ >> $@; \
#	rm -f $@.$$$$

# compile
$(OUTDIR)/%.o:%.c
	@$(MKDIR) -p $(OUTDIR)
	@set -e;rm -f $(@:.o=.d); \
	$(CC) -MM $(CPPFLAGS) $< > $(@:.o=.d).$$$$; \
	sed 's,\($*\)\.o[ :]*,$(OUTDIR)/\1.o $@ : ,g' < $(@:.o=.d).$$$$ > $(@:.o=.d); \
	sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
		-e '/^$$/ d' -e 's/$$/ :/' < $(@:.o=.d).$$$$ >> $(@:.o=.d); \
	rm -f $(@:.o=.d).$$$$; 
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@

.PHONY:clean package
clean:
	@echo clean project...
	-rm -rf $(OUTDIR)

package : $(PACKAGE)
