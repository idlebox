#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mount.h>
#include <string.h>
#include <ftw.h>

#include "log.h"
#include "device.h"


void open_devnull_stdio(void)
{
	int fd;
	static const char *name = "/dev/__null__";
	if (mknod(name, S_IFCHR | 0600, (1 << 8) | 3) == 0) {
		fd = open(name, O_RDWR);
		unlink(name);
		if (fd >= 0) {
			dup2(fd, 0);
			dup2(fd, 1);
			dup2(fd, 2);
			if (fd > 2) {
				close(fd);
			}
			return;
		}
	}
	exit(1);
}


int printfile(const char*fpath, const struct stat *sb, int typeflag) {
	INFO("%s", fpath);
	switch(typeflag) {
		case FTW_F:
			INFO("\n");
			break;
		case FTW_D:
			INFO("/\n");
			break;
		case FTW_DNR:
		case FTW_NS:
			INFO("--SOME THING WRONG\n");
			break;
	}
	return 0;
}

int main(int argc, char **argv)
{
	printf("****************************init start****************************\n");
	int device_fd = -1;

	umask(0);
	open_devnull_stdio();
	log_init();

	mkdir("/dev", 0755);
	mkdir("/sys", 0755);
	mount("sysfs", "/sys", "sysfs", 0, NULL);
	/*
	   mkdir("/proc", 0755);

	   mount("tmpfs", "/dev", "tmpfs", 0, "mode=0755");
	   mkdir("/dev/pts", 0755);
	   mkdir("/dev/socket", 0755);
	   mount("devpts", "/dev/pts", "devpts", 0, NULL);
	   mount("proc", "/proc", "proc", 0, NULL);
	   */

	INFO("device init\n");
	device_fd = device_init();

	/*char buf[512];
	memset(buf, 0, 512);
	read(0, buf, 511);
	printf(buf);*/
	ftw("/dev", printfile, 1000);

	INFO("****************************init start****************************\n");
	sleep(5000000);

	return 0;
}
