#ifndef _DEVICE_H
#define _DEVICE_H

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <dirent.h>

struct uevent {
	const char *action;
	const char *path;
	const char *subsystem;
	const char *firmware;
	int major;
	int minor;
};

int device_init(void);
int open_uevent_socket(void);
void coldboot(int event_fd, const char *path);
void do_coldboot(int event_fd, DIR *d);
void handle_device_fd(int fd);
void parse_event(const char *msg, struct uevent *uevent);
void handle_device_event(struct uevent *uevent);
void make_device(const char *path, int block, int major, int minor);
mode_t get_device_perm(const char *path, unsigned *uid, unsigned *gid);

#endif
